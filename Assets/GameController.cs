﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Linq;
using TMPro;
using System.Text;
using System;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{

    public class Position
    {
        public int? X { get; set; }
        public int? Y { get; set; }
        public int Value { get; set; }
    }

    public class Best
    {
        public int ColumnAI { get; set; }
        public int ColumnPlayer { get; set; }
        public double FitnessValue { get; set; }
        public double ValueAI { get; set; }
        public double ValuePlayer { get; set; }
    }

    public GameObject RedDisc, YellowDisc;
    public Transform[] SpawnPosition;
    public bool PlayerORAI = true; 

    public List<List<Position>> Solution = new List<List<Position>>();

    public List<List<Position>> AllPaterrnPlayer = new List<List<Position>>(); 
    public List<List<Position>> AllPaterrnAI = new List<List<Position>>();

    public string[,] grids = new string[6, 7] {
        {"E", "E", "E", "E", "E", "E", "E",} ,
        {"E", "E", "E", "E", "E", "E", "E",} ,
        {"E", "E", "E", "E", "E", "E", "E",} ,
        {"E", "E", "E", "E", "E", "E", "E",} ,
        {"E", "E", "E", "E", "E", "E", "E",} ,
        {"E", "E", "E", "E", "E", "E", "E",}
    };

    public int[,] PositionList = new int[6, 7];
    public int[] CurrRow = {5,5,5,5,5,5,5};
    

    // Scene 
    [SerializeField]
    private TMP_Text TurnStatus , WhoWin ;
    [SerializeField]
    private GameObject WinAlert ;

    public void SpanwDisc(int row, int column)
    {

        Debug.Log("input : " + row + " " + column);

        GameObject Disc;

        if (PlayerORAI){
            Disc = RedDisc;
        } else {
            Disc = YellowDisc;
        }

        if (grids[row, column] == "E")
        { 
            bool c = true;
            for (int i = 5; i > 0; i--) {
                if (grids[i, column] == "E") {
                    if (c) {
                        row = i;
                    }
                    c = false;
                }
            }

            if (c) {
                row = 0;
            } 

            GameObject obj = Instantiate(Disc, SpawnPosition[column]);
            obj.GetComponent<Transform>().rotation = new Quaternion(0f, 90f, 0f, 0);
            obj.name = (row + 1) + "" + (column + 1);
            
            if (PlayerORAI)
            {
                grids[row, column] = "P";
            } else {
                grids[row, column] = "A";
            }

            // CheckingWinner return E = not win , P = Player win , A = AI Win 
            if (CheckingWinner()=="A")
            {
                ShowWinDialog("ปัญญาประดิษฐ์ เป็น ผู้ชนะ");
                Debug.Log("AI Win");
                return;
            }
            else if (CheckingWinner() == "P")
            {
                ShowWinDialog("ผู้เล่น เป็น ผู้ชนะ");
                Debug.Log("Player Win");
                return;
            }

            PlayerORAI = !PlayerORAI;
        }

        Debug.Log("output : " + row + " " + column);
        CurrRow[column]-- ;  

        if (PlayerORAI)
        {
            TurnStatus.text = "ผู้เล่น" ;
        }else{
            TurnStatus.text = "ปัญญาประดิษฐ์" ;
            StartCoroutine(WaitingForAI());
        }


 
    } 

    IEnumerator WaitingForAI(){
        int col = DecidedByAI();
        yield return new WaitForSeconds(1.0f);
        SpanwDisc(0, col);
        //Debug.Log(DecidedByAI());
    }

    string CheckingWinner()
    {
        string winner = "E";

        FullScan("P", ref AllPaterrnPlayer,grids);
        FullScan("A", ref AllPaterrnAI, grids);

        if (AllPaterrnAI.Any(a=>a.Count(aa=>aa.Value==0)==0))
        {
            winner = "A";
        }

        if (AllPaterrnPlayer.Any(p=>p.Count(pp=>pp.Value==0)==0))
        {
            winner = "P";
        }

        return winner;
    }

    void ShowWinDialog(string winner){
        WinAlert.SetActive(true);
        WhoWin.text = winner ;
    }

    int DecidedByAI(){

        int decidedByAI = 0 ;

        List<Best> globalBest = new List<Best>();

        for (int a = 0; a < CurrRow.Length; a++)
        {
            int[] currRowTemp = (int[])CurrRow.Clone();

            if (currRowTemp[a] < 0) continue;

            List<Best> pLocalBest = new List<Best>();
            List<Best> aLocalBest = new List<Best>();

            string[,] gridsAITemp = (string[,])grids.Clone();

            

            int row = currRowTemp[a];
            int column = a;


            //Debug.Log($"Start Column {a}");

            gridsAITemp[row, column] = "A";
            currRowTemp[a]--;

            //Debug.Log($"Try to play {a} : {row},{column}");

            for (int p = 0; p < currRowTemp.Length; p++)
            {
                string[,] gridsPlayerTemp = (string[,])gridsAITemp.Clone();

                if (currRowTemp[p] < 0) continue;

                gridsPlayerTemp[currRowTemp[p], p] = "P";
                currRowTemp[p]--;

                AllPaterrnPlayer = new List<List<Position>>();
                AllPaterrnAI = new List<List<Position>>();


                FullScan("P", ref AllPaterrnPlayer,gridsPlayerTemp);
                FullScan("A", ref AllPaterrnAI, gridsPlayerTemp);

                if (AllPaterrnPlayer.Count > 0)
                {
                    AllPaterrnPlayer.ForEach(patt =>
                    {
                        Solution.ForEach(sol =>
                        {
                            double result = Minkowski_Distance(sol.Select(x => x).ToList(), patt.Select(x => x).ToList());
                            pLocalBest.Add(new Best { ColumnAI = a, ColumnPlayer = p, FitnessValue = result });
                        });
                    });
                }

                if (AllPaterrnAI.Count > 0)
                {
                    AllPaterrnAI.ForEach(patt =>
                    {
                        Solution.ForEach(sol =>
                        {
                            double result = Minkowski_Distance(sol.Select(x => x).ToList(), patt.Select(x => x).ToList());
                            aLocalBest.Add(new Best { ColumnAI = a, ColumnPlayer = p, FitnessValue = result });
                        });
                    });
                }
            }

            //Debug.Log($"AllPaterrnPlayer.Count {a} : {AllPaterrnPlayer.Count}" );
            //Debug.Log($"AllPaterrnAI.Count {a} : {AllPaterrnAI.Count}");

            if (pLocalBest.Count > aLocalBest.Count)
            {
                pLocalBest.ForEach(pl =>
                {
                    Best whereAI = aLocalBest.FirstOrDefault(ai => ai.ColumnAI == pl.ColumnAI && ai.ColumnPlayer == pl.ColumnPlayer);
                    globalBest.Add(new Best
                    {
                        ColumnAI = pl.ColumnAI,
                        ColumnPlayer = pl.ColumnPlayer,
                        ValueAI = whereAI != null ? whereAI.FitnessValue : double.MaxValue,
                        ValuePlayer = pl.FitnessValue
                    });
                });
            }
            else
            {
                aLocalBest.ForEach(ai =>
                {
                    Best wherePlayer = pLocalBest.FirstOrDefault(pl => pl.ColumnAI == ai.ColumnAI && pl.ColumnPlayer == ai.ColumnPlayer);
                    globalBest.Add(new Best
                    {
                        ColumnAI = ai.ColumnAI,
                        ColumnPlayer = ai.ColumnPlayer,
                        ValuePlayer = wherePlayer != null ? wherePlayer.FitnessValue : double.MaxValue,
                        ValueAI = ai.FitnessValue
                    });
                });
            }

            #region Old Version
            //Best minA = aLocalBest.OrderBy(x => x.FitnessValue).FirstOrDefault();
            //Best maxA = aLocalBest.OrderByDescending(x => x.FitnessValue).FirstOrDefault();

            //Best minP = pLocalBest.OrderBy(x => x.FitnessValue).FirstOrDefault();
            //Best maxP = pLocalBest.OrderByDescending(x => x.FitnessValue).FirstOrDefault();

            //minA = minA == null ? new Best { FitnessValue = double.MaxValue } : minA;
            //maxA = maxA == null ? new Best { FitnessValue = 0 } : maxA;

            //minP = minP == null ? new Best { FitnessValue = double.MaxValue } : minP;
            //maxP = maxP == null ? new Best { FitnessValue = 0 } : maxP;

            //Debug.Log("minA : Column " + minA.ColumnBefore + " Value " + minA.FitnessValue);
            //Debug.Log("maxA : Column " + maxA.ColumnBefore + " Value " + maxA.FitnessValue);

            //Debug.Log("minP : Column " + minP.ColumnBefore + " Value " + minP.FitnessValue);
            //Debug.Log("maxP : Column " + maxP.ColumnBefore + " Value " + maxP.FitnessValue);

            //globalBest.Add(new Best {
            //    Column= minP.FitnessValue < minA.FitnessValue ? minP.Column : a,
            //    FitnessValue= minP.FitnessValue < minA.FitnessValue ? minP.FitnessValue : minA.FitnessValue
            //});

            //globalBest.Add(new Best
            //{
            //    ColumnBefore = minP.FitnessValue < minA.FitnessValue ? minP.ColumnBefore : a,
            //    FitnessValue = minP.FitnessValue < minA.FitnessValue ? minP.FitnessValue : minA.FitnessValue
            //});
            #endregion

            //Debug.Log($"Stop Column {a}");
        }

        Debug.Log(globalBest.Count);


        if (globalBest.Count == 0)
        {
            decidedByAI = UnityEngine.Random.Range(0, 6);
            Debug.Log("Random 1");
        }
        else
        {
            #region Old Version
            //Best maxValue = globalBest.OrderByDescending(x => x.FitnessValue).FirstOrDefault();

            //Best win = globalBest.Where(x => x.FitnessValue == 0).FirstOrDefault();

            //decidedByAI = win != null ? win.Column : maxValue.Column;
            #endregion

            double minAI = globalBest.Min(g => g.ValueAI) ;
            double minPL = globalBest.Min(g => g.ValuePlayer);



            List<Best> minGloblebest = new List<Best>();

            if (minAI==0)
            {
                minGloblebest = globalBest.Where(g => g.ValueAI == minAI).ToList();
                Debug.Log("minAI "+minAI);
            }
            else
            {
                minGloblebest = globalBest.Where(g => g.ValuePlayer == minPL).ToList();
                Debug.Log("minPL "+minPL);
            }

            Debug.Log(minGloblebest.Count);

            if (minGloblebest.Count>0)
            {
                int rand = UnityEngine.Random.Range(0, minGloblebest.Count - 1);

                decidedByAI = minAI == 0 ? minGloblebest[rand].ColumnAI : minGloblebest[rand].ColumnPlayer;
                //decidedByAI = minGloblebest[rand].ColumnPlayer;
            }
            else
            {
                decidedByAI = UnityEngine.Random.Range(0, 6);
                Debug.Log("Random 2");
            }
        }

        return decidedByAI;
    }

    double Minkowski_Distance(List<Position> pos1 , List<Position> pos2 )
    {

        pos1 = pos1.OrderBy(x => x.Value).ToList();
        pos2 = pos2.OrderBy(x => x.Value).ToList();

        int p = pos1.Count < pos2.Count ? pos1.Count : pos2.Count;

        float totle = 0.0f;

        for (int i=0; i<p;i++)
        {
            totle += Mathf.Pow(Mathf.Abs(pos1[i].Value - pos2[i].Value), 1.0f / p);
        }

        return Mathf.Pow(totle,p);
    }

    void FullScan(string who, ref List<List<Position>> pattern,string[,] grid)
    {
        for (int row = 5; row >=0 ; row--)
        {
            for (int col = 0; col <=6; col++)
            {
                FindNearPattern(who, row, col, ref pattern, grid);
            }
        }
    }

    void FindNearPattern(string who,int row, int column, ref List<List<Position>> pattern,string[,] grid)
    {
        // Debug.Log(" FindNearPattern " + row);
        List<Position> pn;
        bool samePattern;

        #region Vertical
        // vertical
        if (row - 3>=0)
        {
            samePattern = true;
            pn = new List<Position>();
            for (int i = 0; i < 4; i++)
            {
                if (grid[row - i, column] == who)
                {
                    // Debug.Log(row + "" + column + " vertical " + i + " " + column + " = " + gridsTemp[i,column]);
                    pn.Add(new Position { X = row - i, Y = column, Value = PositionList[row - i, column] });
                }
                else if (grid[row - i, column]!="E")
                {
                    samePattern = false;
                }
                else
                {
                    pn.Add(new Position { X = null, Y = null, Value = 0 });
                }
            }
            if (samePattern)
            {
                pattern.Add(pn.Select(x => x).ToList());
            }
            //Debug.Log("Vertical : ["+string.Join(",",pn.Select(patt=>patt.Value))+"]");
        }
        #endregion

        #region Horizontal
        // Horizontal
        if (column+3<=6)
        {
            samePattern = true;
            pn = new List<Position>();
            for (int i = 0; i < 4; i++)
            {
                if (grid[row, column + i] == who)
                {
                    pn.Add(new Position { X = row, Y = column + i, Value = PositionList[row, column + i] });
                }
                else if (grid[row, column + i]!="E")
                {
                    samePattern = false;
                }
                else
                {
                    pn.Add(new Position { X = null, Y = null, Value = 0 }); 
                }
            }
            if (samePattern)
            {
                pattern.Add(pn.Select(x => x).ToList());
            }
            //Debug.Log("Horizontal : [" + string.Join(",", pn.Select(patt => patt.Value)) + "]");
        }
        #endregion

        #region Diagonal bottom right to top left
        // diagonal bottom right to top left
        if (row-3 >= 0 && column-3 >= 0)
        {
            samePattern = true;
            pn = new List<Position>();
            for (int i = 0; i < 4; i++)
            {
                if (grid[row - i, column - i] == who)
                {
                    pn.Add(new Position { X = row - i, Y = column - i, Value = PositionList[row - i, column - i] });
                }
                else if (grid[row - i, column - i]!="E")
                {
                    samePattern = false;
                }
                else
                {
                    pn.Add(new Position { X = null, Y = null, Value = 0 });
                }
            }
            if (samePattern)
            {
                pattern.Add(pn.Select(x => x).ToList());
            }
            //Debug.Log("Diagonal bottom right to top left : [" + string.Join(",", pn.Select(patt => patt.Value)) + "]");
        }
        #endregion

        #region Diagonal bottom left to top right
        // diagonal bottom left to top right
        if (row-3 >= 0 && column+3 <= 6)
        {
            samePattern = true;
            pn = new List<Position>();
            for (int i = 0; i < 4; i++)
            {
                if (grid[row - i, column + i] == who)
                {
                    pn.Add(new Position { X = row - i, Y = column + i, Value = PositionList[row - i, column + i] });
                }
                else if (grid[row - i, column + i]!="E")
                {
                    samePattern = false;
                }
                else
                {
                    pn.Add(new Position { X = null, Y = null, Value = 0 });
                }
            }
            if (samePattern)
            {
                pattern.Add(pn.Select(x => x).ToList());
            }
            //Debug.Log("Diagonal bottom left to top right : [" + string.Join(",", pn.Select(patt => patt.Value)) + "]");
        }
        #endregion

        pattern = pattern.Where(patt => patt.Where(pos => pos.Value!=0).Count() > 1).ToList();

    }

    void Start()
    {

        GridWithValue_Init();

        findSolution();

    }

    void GridWithValue_Init()
    {
        int c = 0;
        for (int i = 0; i < 6; i++)
        {
            for (int j = 0; j < 7; j++)
            {
                c++;
                PositionList[i, j] = c;
            }
        }
    }

    void findSolution()
    {
        Solution = new List<List<Position>>();

        for (int row = 5; row >= 0; row--)
        {
            for (int col = 0; col <= 6; col++)
            {

                #region Vertical
                List<Position> patt = new List<Position>();
                if (row - 3 >= 0)
                {
                    for (int i = 0; i < 4; i++)
                    {
                        patt.Add(new Position { X = row - i, Y = col,Value=PositionList[row - i,col] });
                    }
                    Solution.Add(patt);
                }
                #endregion

                #region horizontal
                // horizontal
                patt = new List<Position>();
                if (col + 3 <= 6)
                {
                    for (int i = 0; i < 4; i++)
                    {
                        patt.Add(new Position { X = row, Y = col + i, Value = PositionList[row, col+i] });
                    }
                    Solution.Add(patt);
                }
                #endregion

                #region Diagonal bottom right to top left
                // Diagonal bottom right to top left
                patt = new List<Position>();
                if (row - 3 >=0 && col - 3 >= 0)
                {
                    for (int i = 0; i < 4; i++)
                    {
                        patt.Add(new Position { X = row - i, Y = col - i, Value = PositionList[row - i, col-i] });
                    }
                    Solution.Add(patt);
                }
                #endregion

                #region Diagonal bottom left to top right
                // Diagonal bottom left to top right
                patt = new List<Position>();
                if (row - 3 >= 0 && col + 3 <= 6)
                {
                    for (int i = 0; i < 4; i++)
                    {
                        patt.Add(new Position { X = row - i, Y = col + i, Value = PositionList[row - i, col+i] });
                    }
                    Solution.Add(patt);
                }
                #endregion
            }
        }

        Debug.Log(Solution.Count);
    }

   private void Update() { 
       if (Input.GetKeyDown(KeyCode.Space))
       { 

            int rowLength = grids.GetLength(0);
            int colLength = grids.GetLength(1);

            for (int i = 0; i < rowLength; i++)
            {
                StringBuilder builder = new StringBuilder();
                for (int j = 0; j < colLength; j++)
                {  
                    builder.AppendFormat(string.Format("{0} ", grids[i, j]), Environment.NewLine);
                }
                Debug.Log(builder.ToString());
            }
       }
   }  

   public void RestartGame(){
       SceneManager.LoadScene("GamePlay");
   }
} 